#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

void add_config() {
    char *path = "/usr/local/sbin/slapadd";
    char *args[] = {path, "-d", "256", "-n0", "-F", "/usr/local/etc/slapd.d", "-l", "/usr/local/etc/openldap/config.ldif",  NULL};

    execv(path, args);
  	exit(EXIT_FAILURE);
}

void add_data() {
    char *path = "/usr/local/sbin/slapadd";
    char *args[] = {path, "-d", "256", "-n1", "-F", "/usr/local/etc/slapd.d", "-l", "/usr/local/etc/openldap/data.ldif",  NULL};

    execv(path, args);
    exit(EXIT_FAILURE);
}

int main(void) {
    pid_t pid_config = fork();
    if (pid_config == 0) { add_config(); }

    int status_config;
    while (wait(&status_config) !=  pid_config);

    pid_t pid_data = fork();
    if (pid_data == 0) { add_data(); }

    int status_data;
    while (wait(&status_data) !=  pid_data);

    char *path = "/usr/local/libexec/slapd";
    char *args[] = {path, "-d", "256", "-F", "/usr/local/etc/slapd.d", "-h", "ldap://*:389",  NULL};

    execv(path, args);
    
    /* execv() returns only on error */
    exit(EXIT_FAILURE);
}
